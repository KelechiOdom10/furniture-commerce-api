namespace API.DTOs;

public class BaseDto
{
    public int Id { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
}